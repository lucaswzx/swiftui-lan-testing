//
//  LANConnector.swift
//  networking_LAN_test
//
//  Created by Lucas Wang on 2021-06-24.
//

import Foundation
import Network
import Combine
import UniformTypeIdentifiers

// Publisher used by view(s)'s .onReceive to update received value/String accordingly
let publisherUpdate = PassthroughSubject <String, Never> ()
let publisherFileReceivedUpdate = PassthroughSubject <Data, Never> ()

// LANConnector class that handles all LAN activities, see ContentView.swift for example usage
class LANConnector: NSObject {
    private var sender: NWConnection?
    private var listener: NWListener?
    private var connectionType: NWParameters = .udp // starts off with UDP, can be changed
    
    func setToUDP() {
        connectionType = .udp
    }
    
    func setToTCP() {
        connectionType = .tcp
    }
    
    // Listens on the specified port
    func listen(port: NWEndpoint.Port) {
        do {
            self.listener = try NWListener(using: connectionType, on: port)
            self.listener?.stateUpdateHandler = {(newState) in
                switch newState {
                    case .ready: print("ready")
                    default: break
                }
            }
            self.listener?.newConnectionHandler = {(newConnection) in
                newConnection.stateUpdateHandler = {newState in
                    switch newState {
                        case .ready:
                            print("new connection")
                            self.receive(on: newConnection)
                        default: break
                    }
                }
                newConnection.start(queue: DispatchQueue(label: "new client"))
            }
        } catch {
            print("unable to create listener")
        }
        self.listener?.start(queue: .main)
    }
    
    // Private helper function for listener, used by listen
    private func receive(on connection: NWConnection) {
        connection.receiveMessage { (data, context, isComplete, error) in
            if let error = error {
                print(error)
                return
            }
            if let data = data, !data.isEmpty {
                let backToString = String(decoding: data, as: UTF8.self)
                print("b2S", backToString)
                DispatchQueue.main.async {
                    publisherUpdate.send(backToString)
                }
            }
        }
    }
    
    private func receiveData(on connection: NWConnection) {
        connection.receiveMessage { (data, context, isComplete, error) in
            if let error = error {
                print(error)
                return
            }
            if let data = data, !data.isEmpty {
                DispatchQueue.main.async {
                    publisherFileReceivedUpdate.send(data)
                }
            }
        }
    }
    
    func listenData(port: NWEndpoint.Port) {
        do {
            self.listener = try NWListener(using: connectionType, on: port)
            self.listener?.stateUpdateHandler = {(newState) in
                switch newState {
                    case .ready: print("ready")
                    default: break
                }
            }
            self.listener?.newConnectionHandler = {(newConnection) in
                newConnection.stateUpdateHandler = {newState in
                    switch newState {
                        case .ready:
                            print("new connection")
                            self.receiveData(on: newConnection)
                        default: break
                    }
                }
                newConnection.start(queue: DispatchQueue(label: "new client"))
            }
        } catch {
            print("unable to create listener")
        }
        self.listener?.start(queue: .main)
    }
    
    // Connect to the specified host/target address at the specified port
    func connect(targetAddress: NWEndpoint.Host, targetPort: NWEndpoint.Port) {
        self.sender = NWConnection(host: targetAddress, port: targetPort, using: connectionType)
        self.sender?.stateUpdateHandler = { (newState) in
            switch(newState) {
                case .ready: break
                default: break
            }
        }
        self.sender?.start(queue: .main)
    }
    
    // Send the given String to the specified host/target address at the specified port
    func send(_ content: String) {
        let contentToSend = content.data(using: String.Encoding.utf8)
        self.sender?.send(content: contentToSend,
                           completion: NWConnection.SendCompletion.contentProcessed(({
                            (NWError) in
                            if (NWError == nil) {
                                print("Sent successfully!")
                            } else {
                                print("ERROR when sending! NWError: \n \(NWError!) ")
                            }
                           })))
    }
    
    // Send the file at the given URL (directory)...
    func sendFile(_ fileURL: URL) {
        do {
            // start/stopAccessingSecurityScopedResource() is needed otherwise we get permission errors
            fileURL.startAccessingSecurityScopedResource()
            let contentToSend: Data = try Data(contentsOf: fileURL)
            print(contentToSend) //printing type: Data prints its size in bytes to the console
            fileURL.stopAccessingSecurityScopedResource()
            // otherwise, once converted to type: Data, send procedure is exactly the same as for Strings as shown in the previous function above
            self.sender?.send(content: contentToSend,
                              completion: NWConnection.SendCompletion.contentProcessed(({
                                (NWError) in
                                if (NWError == nil) {
                                    print("Sent successfully!")
                                } else {
                                    print("ERROR when sending! NWError: \n \(NWError!) ")
                                }
                              })))
        } catch {
            print("Error: \(error)")
        }
    }
}
