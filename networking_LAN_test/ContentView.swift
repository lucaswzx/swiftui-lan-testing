//
//  ContentView.swift
//  networking_LAN_test
//
//  Created by Lucas Wang on 2021-06-24.
//

import SwiftUI
import Network
import Combine
import UniformTypeIdentifiers
import UIKit

struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    
    @State var receivedMessage: String = ""
    @State var messageToSend: String = ""
    @State var listeningPortNum: String = ""
    @State var sendingTargetLANAddress: String = ""
    @State var sendingTargetPortNum: String = ""
    @State var selectedConnectionTypeIndex: Int = 0
    @State var showingFilePicker: Bool = false
    @State var chosenFileURLs: [URL] = []
    @State var newFileName: String = ""
            
    func onConnectionTypeChange(_ tag: Int) {
        if (tag == 0) {
            communication.setToUDP()
        } else if (tag == 1) {
            communication.setToTCP()
        }
    }
    
    // 0 = Message, 1 = file
    @State var receivingTypeIndex: Int = 0
        
    // LANConnector class that handles all LAN activities, see below for example usage
    let communication = LANConnector()
    
    // defines the LAN address:Port of the target to receive the sent data
    // Everything initialized to 0 or "" at first, needs user input to start connecting
    @State var sendingTarget = NWEndpoint.Host.init("")
    @State var sendingPort = NWEndpoint.Port.init(integerLiteral: 9999)
    
    // defines what port to listen on
    @State var listeningPort = NWEndpoint.Port.init(integerLiteral: 9999)
    
    let haptics = UIImpactFeedbackGenerator(style: .heavy)
    
    var body: some View {
        NavigationView {
            List{
                Section(header: Text("Connection Type (requires re-Setup)")) {
                    Picker(selection: $selectedConnectionTypeIndex, label: Text("Connection Type")) {
                        Text("UDP").tag(0)
                        Text("TCP").tag(1)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .onChange(of: selectedConnectionTypeIndex, perform: { value in
                        onConnectionTypeChange(value)
                    })
                }
                
                Section(header: Text("Listener Setup")) {
                    TextField("Listening Port Number", text: $listeningPortNum)
                    Text("Receiving Data Type: (Requires re-setup). If \"File\" is chosen, don't forget to enter a file name in the \"File Receiver\" section!!! Otherwise you get a random 10 character String as the filename.")
                    Picker(selection: $receivingTypeIndex, label: Text("Receiving Data Type")) {
                        Text("Message").tag(0)
                        Text("File").tag(1)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    Button(action: {
                        listeningPort = NWEndpoint.Port.init(integerLiteral: UInt16(listeningPortNum) ?? 9999)
                        if (receivingTypeIndex == 0) {
                            communication.listen(port: listeningPort)
                        } else if (receivingTypeIndex == 1) {
                            communication.listenData(port: listeningPort)
                        }
                        haptics.impactOccurred(intensity: 1.0)
                    }, label: {
                        Text("Setup Port & Start listening")
                            .foregroundColor(.blue)
                    })
                }
                
                Section(header: Text("Sender Setup")) {
                    TextField("Receiving target LAN address", text: $sendingTargetLANAddress)
                    TextField("Receiving target Port", text: $sendingTargetPortNum)
                    
                    Button(action: {
                        sendingTarget = NWEndpoint.Host.init(sendingTargetLANAddress)
                        sendingPort = NWEndpoint.Port.init(integerLiteral: UInt16(sendingTargetPortNum) ?? 9999)
                        communication.connect(targetAddress: sendingTarget, targetPort: sendingPort)
                        haptics.impactOccurred(intensity: 1.0)
                    }, label: {
                        Text("Setup Sender")
                            .foregroundColor(.blue)
                    })
                }
                
                Section(header: Text("Listener")) {
                    Text("Received Message: \(receivedMessage)")
                }
                
                Section(header: Text("File Receiver")) {
                    TextField("New File Name", text: $newFileName)
                }
                
                Section(header: Text("Text Message Sender")) {
                    TextField("Message to send", text: $messageToSend)
                    Button(action: {
                        communication.send("\(messageToSend)\n")
                        haptics.impactOccurred(intensity: 1.0)
                    }, label: {
                        Text("Send Message")
                            .foregroundColor(.blue)
                    })
                }
                
                Section(header: Text("File(s) Sender")) {
                    Button(action: {
                        showingFilePicker = true
                    }, label: {
                        Text("Import File(s)")
                            .foregroundColor(.blue)
                    })
                    Text("Chosen file URL(s):")
                    ForEach(chosenFileURLs, id: \.self) { url in
                        Text(url.absoluteString)
                    }
                    Button(action: {
                        for url in chosenFileURLs {
                            communication.sendFile(url)
                            haptics.impactOccurred(intensity: 1.0)
                        }
                    }, label: {
                        Text("Send Chosen File(s)")
                            .foregroundColor(.blue)
                    })
                }
                
                Section(header: Text("README")) {
                    Text("LAN Tester is an app for testing LAN communications that essentially functions similar to the netcat (nc) command line tool. Enter the respective required information (address, port, type) and then tap the respective setup button to setup the listener AND/OR the sender. Tap setup again when any of those information is changed.\n")
                    Text("Communication can be done from any other device with the netcat utility. Use:\n\n echo -n \"message here\" | nc ADDR PORT\n\n to send to a specific device, and use\n\n nc -l to listen.\n")
                    Text("*** netcat defaults to using TCP for both sending and listening, to send and listen using UDP, add the -u flag to both commands above (and also set the app to UDP and tap all the setup buttons again) ***\n")
                    Text("UDP multicast broadcasting can be done with netcat by using:\n\n nc -ub 255.255.255.255 PORT\n\n This can be done in the LAN Tester app by simply using the Sender with the same ADDR and PORT. Note that in order for broadcast to work on a physical iOS device running this app, an entitlement is REQUIRED, otherwise it will work in the Simulator ONLY.\n")
                    Text("Even though the File(s) Sender supports selecting and sending multiple\nfiles with one tap, I'm not sure how to do this with netcat without a shell script.\nBut to receive a single file with netcat:\n\nnc -l (add -u for UDP/smaller files) > /dir/to/received/file.extensionDesired\n\nIf you do this and send multiple files, netcat would still work. But it would simply jumble all the binary data of those multiple files inside that single file that was receiving input from netcat, so it would end up corrupted.")
                }
                
                Section() {
                    Button(action: {
                        //testDecode()
                        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].absoluteString)
                    }, label: {
                        Text("Test, check console")
                            .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    })
                }
                
            }
            .navigationTitle("LAN Tester")
        }
        .onChange(of: scenePhase) { newPhase in
            switch (newPhase) {
                case .inactive: print("App entered inactive state")
                case .active: print("App entered foreground")
                case .background:
                    print("App entered background")
                    scheduleHapticInBackground()
                default: print("How did we get here?")
            }
        }
        .onReceive(publisherUpdate, perform: { value in
            // When listener updates its value/receives new message, triggers code here and updates view and values accordingly
            if (receivingTypeIndex == 0) {
                receivedMessage = value
            }
        })
        .onReceive(publisherFileReceivedUpdate, perform: { value in
            if (receivingTypeIndex == 1) {
                if (saveFile(fileData: value, filename: (newFileName == "" ? randomString(length: 10) : newFileName))) {
                    print("File received successfully")
                } else {
                    print("File receiving error")
                }
            }
        })
        .fileImporter(isPresented: $showingFilePicker, allowedContentTypes: allUTTypes, allowsMultipleSelection: true) { result in
            do {
                chosenFileURLs = try result.get()
            } catch {
                print("Document Picker Error")
            }
        }
    }
}
