//
//  networking_LAN_testApp.swift
//  networking_LAN_test
//
//  Created by Lucas Wang on 2021-06-24.
//

import SwiftUI
import BackgroundTasks

@main
struct networking_LAN_testApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onAppear() {
                    BGTaskScheduler.shared.register(forTaskWithIdentifier: "kdeconnect.backgroundListener", using: nil) { (task) in
                        periodicHaptics(task: task as! BGAppRefreshTask)
                    }
                }
        }
    }
}
