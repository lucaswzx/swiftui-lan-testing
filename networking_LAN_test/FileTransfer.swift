//
//  FileTransfer.swift
//  networking_LAN_test
//
//  Created by Lucas Wang on 2021-07-18.
//

import Foundation
import UniformTypeIdentifiers

// Array of all UTTypes, used by .fileImporter() to allow importing of all file types
let allUTTypes: [UTType] = [.aiff, .aliasFile, .appleArchive, .appleProtectedMPEG4Audio,
                            .appleProtectedMPEG4Video, .appleScript, .application,
                            .applicationBundle, .applicationExtension, .arReferenceObject,
                            .archive, .assemblyLanguageSource, .audio, .audiovisualContent,
                            .avi, .binaryPropertyList, .bmp, .bookmark, .bundle, .bz2,
                            .cHeader, .cPlusPlusHeader, .cPlusPlusSource, .cSource,
                            .calendarEvent, .commaSeparatedText, .compositeContent,
                            .contact, .content, .data, .database, .delimitedText, .directory,
                            .diskImage, .emailMessage, .epub, .exe, .executable, .fileURL,
                            .flatRTFD, .folder, .font, .framework, .gif, .gzip, .heic, .html,
                            .icns, .ico, .image, .internetLocation, .internetShortcut, .item,
                            .javaScript, .jpeg, .json, .livePhoto, .log, .m3uPlaylist,
                            /**.makefile (iOS 15 beta),**/ .message, .midi, .mountPoint, .movie, .mp3,
                            .mpeg, .mpeg2TransportStream, .mpeg2Video, .mpeg4Audio,
                            .mpeg4Movie, .objectiveCPlusPlusSource, .objectiveCSource,
                            .osaScript, .osaScriptBundle, .package, .pdf, .perlScript,
                            .phpScript, .pkcs12, .plainText, .playlist, .pluginBundle, .png,
                            .presentation, .propertyList, .pythonScript, .quickLookGenerator,
                            .quickTimeMovie, .rawImage, .realityFile, .resolvable, .rtf, .rtfd,
                            .rubyScript, .sceneKitScene, .script, .shellScript, .sourceCode,
                            .spotlightImporter, .spreadsheet, .svg, .swiftSource,
                            .symbolicLink, .systemPreferencesPane, .tabSeparatedText, .text,
                            .threeDContent, .tiff, .toDoItem, .unixExecutable, .url,
                            .urlBookmarkData, .usd, .usdz, .utf16ExternalPlainText,
                            .utf16PlainText, .utf8PlainText, .utf8TabSeparatedText, .vCard,
                            .video, .volume, .wav, .webArchive, .webP, .x509Certificate, .xml,
                            .xmlPropertyList, .xpcService, .yaml, .zip]

func saveFile(fileData: Data, filename: String) -> Bool {
    let fileManager = FileManager.default
    do {
        let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false) // gets URL of app's document directory
        let fileURL = documentDirectory.appendingPathComponent(filename) // adds new file's name to URL
        //print(fileURL.absoluteString)
        try fileData.write(to: fileURL) // and save!
        return true
    } catch {
        print(error)
    }
    return false
}

func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0..<length).map{ _ in letters.randomElement()! })
}
