//
//  BackgroundTesting.swift
//  networking_LAN_test
//
//  Created by Lucas Wang on 2021-08-12.
//

import UIKit
import BackgroundTasks

let haptics = UIImpactFeedbackGenerator(style: .heavy)

func periodicHaptics(task: BGAppRefreshTask) -> Void {
    scheduleHapticInBackground()
    
    print("TAP")
    haptics.impactOccurred(intensity: 1.0)
    
    task.expirationHandler = {
        // when task about end, wrap up
    }
    
    task.setTaskCompleted(success: true)
}

func scheduleHapticInBackground() -> Void {
    print("Scheduler ready, triggering event in 5 seconds")
    let request = BGAppRefreshTaskRequest(identifier: "kdeconnect.backgroundListener")
    request.earliestBeginDate = Date(timeIntervalSinceNow: 5) // seconds
    do {
        try BGTaskScheduler.shared.submit(request)
    } catch {
        print("Cannot schedule BGTask \(error)")
    }
}
