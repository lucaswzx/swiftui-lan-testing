//
//  JSONTesting.swift
//  networking_LAN_test
//
//  Created by Lucas Wang on 2021-07-11.
//

import Foundation

struct GenericJSONPacket : Codable {
    let id: Int
    let type: String
}

struct IdentityJSONPacket : Codable {
    let deviceId: String
    let deviceName: String
    let protocolVersion: Int
    let deviceType: String
    let incomingCapabilities: [String]
    let outgoingCapabilities: [String]
    let tcpPort: Int
}

//struct TestingJSONPacket : Codable {
//    let id: Int
//    let type: String
//    let body: NSDictionary
//}

func testDecode() {
    
    let combinedJSONString = """
{"id":1626045099474,"type":"kdeconnect.identity","body":{"deviceId":"141f9eb64b921ecf","deviceName":"Pixel 3a XL","protocolVersion":7,"deviceType":"phone","incomingCapabilities":["kdeconnect.telephony.request_mute","kdeconnect.notification","kdeconnect.ping","kdeconnect.notification.reply","kdeconnect.notification.action","kdeconnect.share.request","kdeconnect.bigscreen.stt","kdeconnect.clipboard.connect","kdeconnect.runcommand","kdeconnect.connectivity_report.request","kdeconnect.contacts.request_all_uids_timestamps","kdeconnect.sms.request_conversations","kdeconnect.telephony.request","kdeconnect.mpris","kdeconnect.sms.request_conversation","kdeconnect.findmyphone.request","kdeconnect.sms.request_attachment","kdeconnect.systemvolume","kdeconnect.mousepad.keyboardstate","kdeconnect.sftp.request","kdeconnect.share.request.update","kdeconnect.notification.request","kdeconnect.mousepad.request","kdeconnect.photo.request","kdeconnect.sms.request","kdeconnect.contacts.request_vcards_by_uid","kdeconnect.mpris.request","kdeconnect.battery.request","kdeconnect.clipboard"],"outgoingCapabilities":["kdeconnect.telephony","kdeconnect.notification","kdeconnect.contacts.response_uids_timestamps","kdeconnect.ping","kdeconnect.share.request","kdeconnect.bigscreen.stt","kdeconnect.clipboard.connect","kdeconnect.connectivity_report","kdeconnect.sftp","kdeconnect.sms.attachment_file","kdeconnect.systemvolume.request","kdeconnect.sms.messages","kdeconnect.mpris","kdeconnect.findmyphone.request","kdeconnect.mousepad.keyboardstate","kdeconnect.contacts.response_vcards","kdeconnect.notification.request","kdeconnect.mousepad.echo","kdeconnect.mousepad.request","kdeconnect.presenter","kdeconnect.photo","kdeconnect.runcommand.request","kdeconnect.mpris.request","kdeconnect.battery","kdeconnect.clipboard"],"tcpPort":1716}}
"""
    
    let splitResult: [String] = bodySeperator(rawJSON: combinedJSONString)
    let genericData = splitResult[0].data(using: .utf8)!
    let bodyData = splitResult[1].data(using: .utf8)!
    let decoder = JSONDecoder()
    let genericPacket = try! decoder.decode(GenericJSONPacket.self, from: genericData)
    let bodyPacket = try! decoder.decode(IdentityJSONPacket.self, from: bodyData)
    
    dump(genericPacket)
    print("\n\n\n\n")
    dump(bodyPacket)
}

func bodySeperator(rawJSON: String) -> [String] {
    var returnPair: [String] = ["", ""]
    let startPattern: String = ",\"body\":"
    for i in 8..<rawJSON.count {
        let pre8Chars = String(rawJSON.dropFirst(i - 8).prefix(8))
        if (pre8Chars == startPattern) { // == 59
            returnPair[0] = String(rawJSON.prefix(i - 8))
            returnPair[1] = String(rawJSON.dropFirst(i))
            break
        }
    }
    returnPair[0] += "}"
    returnPair[1] = String(returnPair[1].dropLast())
    return returnPair
}

//func packetCombiner(genericPacket: String, bodyPacket: String)
